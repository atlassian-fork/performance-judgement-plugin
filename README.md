## Current performance criteria

* Interactions with location tolerance defined via `tolerableInteractions` configuration.
* Exceeding the tolerance in any of these interactions causes a rejecting verdict.
* The location is measured by 1%-[trimmed mean](https://en.wikipedia.org/wiki/Truncated_mean).
* Optional minimum volume applied to each interaction separately.

## Glossary

### Interaction

A symbolic representation of a user-system interaction.
Examples:

* Browser Metrics key
* Soke action name

### Location

In statistics `location` describes where a distribution is located in its domain.
Examples: mean, median, mode and [more](https://en.wikipedia.org/wiki/Central_tendency).

### Tolerance ratio

A proportion of a baseline, which is acceptable to exceed.
Examples:

* If a baseline is 1.00 seconds and its tolerance ratio is 10%, then a result of 0.12 seconds is acceptable.
* If a baseline is 1.00 seconds and its tolerance ratio is 10%, then a result of 1.07 seconds is acceptable.
* If a baseline is 1.00 seconds and its tolerance ratio is 5%, then a result of 1.07 seconds is unacceptable.

### Volume

Shorthand for sample size. Small volume leads to high variability.
For example, repeated tests with small volumes tend to be more flaky than ones with high volume.

## Versioning

This module uses [Semantic Versioning 2.0.0](http://semver.org/spec/v2.0.0.html).

## Public API

The public API consists of Maven plugin API and behavior and the definition of itself.

Maven plugin API consists of:

* groupId
* artifactId
* goals:
  * names
  * behavior
  * parameters:
    * names
    * behavior
    * defaults
    * properties
    * format
  * default phase

Maven plugin API behavior contracts are expressed in Maven plugin descriptor (available via `mvn help:describe`).
Additional behavior contracts are provided via change log.

Relaxing the above definitions is considered backwards-incompatible.

## Change log

### 6.3.0

* (new) Add `baselineLabel` and `experimentLabel` parameter to `judge` goal. Resolve enhancement #2.

### 6.2.0

* (new) Interpret missing interaction data as zero volume. Resolve enhancement #1.

### 6.1.1

* (transparent) Reduce memory usage.

### 6.1.0

* (new) Add `csvReport` parameter to `judge` goal.
* (new) Add `label` field to `tolerableInteractions` elements.

### 6.0.1

* (transparent) Clean dependencies.

### 6.0.0

* (breaking) Remove `judge-log` goal.
* (breaking) Remove `judge-soke-report` goal.
* (new) Add `judge` goal.
  It merges the capabilities of the `5.2.1` `judge-log` and `judge-soke-report` goals.
  The merge allows the plugin to observe all sources of data at once, even if one source causes a rejecting verdict.
  The `baselineDirectory` and `experimentDirectory` parameters change their names, defaults and properties.
  The `baselineReport` and `experimentReport` parameters change their names.
  The `minimumVolume` parameter is no longer optional. It receives a default of `1`. Its property changes.

### 5.2.1

* (transparent) Increase verbosity.

### 5.2.0

* (new) Add `minimumVolume` performance criterion.

### 5.1.0

* (new) Add `judge-soke-report` goal.

### 5.0.1

* (transparent) Increase measurement verbosity.

### 5.0.0

* (breaking) Remove predefined Browser Metrics with location tolerance.
* (new) Add `tolerableInteractions` configuration for `judge-log` goal.

### 4.0.1

* (transparent) Actually increase tolerance for `jira.issue.nav-detail` from 5% to 600%.

### 4.0.0

* (breaking) Increase tolerance for `jira.issue.nav-detail` from 5% to 600%.
  See [RUN-1060](https://bulldog.internal.atlassian.com/browse/RUN-1060) for rationale.

### 3.0.1

* (transparent) Increase verdict verbosity.

### 3.0.0

* (breaking) Increase tolerance for `jira.agile.work` from 5% to 8%.

### 2.0.0

* (breaking) Change performance criteria:
  * Browser Metrics with location tolerance:
    * `jira.project.issue.view-issue`: 5%
    * `jira.agile.work`: 5%
    * `jira.issue.nav-detail`: 5%
    * `jira.dashboard`: 10%
  * Exceeding the tolerance in any of these interactions causes a rejecting verdict.
  * The location is measured by 1%-trimmed mean.
* (breaking) Remove `judge-koto`. It cannot calculate trimmed mean.

### 1.3.0

* (new) Add `judge-log` goal.

### 1.2.0

* (new) Add `maxRetries` parameter to `judge-koto`.

### 1.1.0

* (new) Add Maven plugin goal parameter properties to the public API.
* (new) Expose `judge-koto` parameter properties.

### 1.0.0

* (new) Add `judge-koto` goal.
* (new) Define performance criteria:
  * A set of user interactions is predefined:
    * Browser Metrics:
      * `jira.project.issue.view-issue`
      * `jira.agile.work`
      * `jira.issue.nav-detail`
      * `jira.dashboard`
  * Regression in any of these interactions causes a rejecting verdict.
  * A result has to exceed the baseline by more than **5%** to be considered a regression.
def regressionFound = false

def buildLog = new File(basedir, "build.log")
buildLog.eachLine { line ->
    if (line.contains(
            "[WARNING] The 'JIRA 7.2.5' location PT2.79099947S is a regression against 'JIRA 7.2.0' location PT2.786095227S for " +
                    "Interaction(key=add comment, toleranceRatio=1.0E-5, label=add comment)"
    )) {
        regressionFound = true
    }
}

if (!regressionFound) {
    throw new RuntimeException("The expected regression was not found in " + buildLog)
}
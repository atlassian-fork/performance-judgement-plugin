def actualReport = new File(basedir, "target/report.csv").text

def expectedReport = """cohort,metric,jira.dashboard,View Board,add comment,create issue dialog,AGGREGATE\r
baseline,location,1645,2047,2786,11688,3147\r
experiment,location,1744,2080,2790,11685,3230\r
baseline,volume,549,791,201,207\r
experiment,volume,510,783,206,207\r
baseline,tolerance,0.1,0.08,0.005,0.005\r
experiment,tolerance,0.1,0.08,0.005,0.005\r
"""

if (expectedReport != actualReport) {
    throw new RuntimeException("The actual report `$actualReport` does not equal the expected report `$expectedReport`")
}

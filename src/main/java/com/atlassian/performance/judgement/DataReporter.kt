package com.atlassian.performance.judgement

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.math3.stat.descriptive.moment.Mean
import org.apache.maven.plugin.logging.Log
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.time.Duration

internal data class DataReporter(
        private val output: File,
        private val log: Log
) {

    fun report(
            criteria: PerformanceCriteria,
            data: Collection<InteractionStats>
    ) {
        log.info("Writing CSV summary of $data to $output")
        BufferedWriter(FileWriter(output)).use {
            reportCsv(criteria, data, it)
        }
    }

    private fun reportCsv(
            criteria: PerformanceCriteria,
            data: Collection<InteractionStats>,
            target: Appendable
    ) {
        val interactionLabels = criteria.interactions.map { it.label }
        val headers = arrayOf("cohort", "metric").plus(interactionLabels).plus("AGGREGATE")
        val format = CSVFormat.DEFAULT.withHeader(*headers)
        val printer = CSVPrinter(target, format)
        printer.printRecords(data.map {
            listOf(it.cohort, "location")
                    .plus(it.locations.values.map { serialize(it) })
                    .plus(aggregateLocations(it))
        })
        printer.printRecords(data.map {
            listOf(it.cohort, "volume").plus(it.volumes.values)
        })
        printer.printRecords(data.map {
            listOf(it.cohort, "tolerance").plus(criteria.interactions.map { it.toleranceRatio })
        })
        printer.flush()
    }

    private fun serialize(it: Duration) = it.toMillis()

    private fun aggregateLocations(it: InteractionStats): Long {
        val aggregate = Mean()
        val locations = it.locations.values.map { serialize(it).toDouble() }.toDoubleArray()
        val volumes = it.volumes.values.map(Long::toDouble).toDoubleArray()
        return aggregate.evaluate(locations, volumes).toLong()
    }
}
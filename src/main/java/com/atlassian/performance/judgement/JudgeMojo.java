package com.atlassian.performance.judgement;


import com.atlassian.performance.judgement.analytics.log.AnalyticLogsReader;
import com.atlassian.performance.judgement.soke.SokeLogExtractor;
import com.atlassian.performance.judgement.soke.SokeLogReader;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Judges performance results:
 * <ul>
 * <li>Succeeds if the results are accepted.</li>
 * <li>Fails if the results are rejected.</li>
 * <li>Allows to read Analytics logs.</li>
 * <li>Allows to read Soke reports.</li>
 * <li>Requires performance criteria.</li>
 * <li>Allows to judge volume.</li>
 * </ul>
 * If any data source is unreadable, it is skipped and by itself does not fail builds.
 * Skipped data source might indirectly cause a build failure due to missing volume or rejected verdict.
 */
@Mojo(name = "judge", defaultPhase = LifecyclePhase.VERIFY)
class JudgeMojo extends AbstractMojo {

    /**
     * Soke report for the baseline cohort.
     */
    @Parameter(
            defaultValue = "${project.build.directory}/baseline-report.zip",
            property = "judge.soke.baseline.report"
    )
    private File baselineSokeReport;

    /**
     * Soke report for the experiment cohort.
     */
    @Parameter(
            defaultValue = "${project.build.directory}/experiment-report.zip",
            property = "judge.soke.experiment.report"
    )
    private File experimentSokeReport;

    /**
     * Directory containing Analytics logs for the baseline cohort.
     */
    @Parameter(
            defaultValue = "${project.build.directory}/baseline-analytics",
            property = "judge.analytics.baseline.directory"
    )
    private File baselineAnalyticsDirectory;

    /**
     * Directory containing Analytics logs for the experiment cohort.
     */
    @Parameter(
            defaultValue = "${project.build.directory}/experiment-analytics",
            property = "judge.analytics.experiment.directory"
    )
    private File experimentAnalyticsDirectory;

    /**
     * Label of the baseline cohort.
     * Visible in the log and the CSV report.
     */
    @Parameter(
            defaultValue = "baseline",
            property = "judge.cohort.label.baseline"
    )
    private String baselineLabel;

    /**
     * Label of the experiment cohort.
     * Visible in the log and the CSV report.
     */
    @Parameter(
            defaultValue = "experiment",
            property = "judge.cohort.label.experiment"
    )
    private String experimentLabel;

    /**
     * Interactions to judge.
     */
    @Parameter
    private List<TolerableInteraction> tolerableInteractions;

    /**
     * The minimum volume each interaction needs to have.
     * If data for an interaction is completely missing, it is considered to have zero volume.
     */
    @Parameter(
            defaultValue = "1",
            property = "judge.criteria.volume.minimum"
    )
    private Long minimumVolume;

    /**
     * CSV report output file.
     */
    @Parameter(
            defaultValue = "${project.build.directory}/report.csv",
            property = "judge.report.csv"
    )
    private File csvReport;

    public void execute() throws MojoExecutionException, MojoFailureException {
        Log log = getLog();
        SokeLogExtractor extractor = new SokeLogExtractor();
        StatsReaderJudge judge = new StatsReaderJudge(
                new PerInteractionJudge(log),
                new VolumeJudge(log),
                new DataReporter(csvReport, log)
        );
        PerformanceCriteria criteria = new PerformanceCriteria(
                tolerableInteractions.stream()
                        .map(TolerableInteraction::becomeProper)
                        .collect(Collectors.toList()),
                minimumVolume
        );
        boolean accepted = judge.judge(
                criteria,
                new StatsMeter(
                        new MergingReader(Arrays.asList(
                                new AnalyticLogsReader(baselineAnalyticsDirectory, log),
                                new SokeLogReader(baselineSokeReport, extractor, log)
                        )),
                        baselineLabel,
                        log
                ),
                new StatsMeter(
                        new MergingReader(Arrays.asList(
                                new AnalyticLogsReader(experimentAnalyticsDirectory, log),
                                new SokeLogReader(experimentSokeReport, extractor, log)
                        )),
                        experimentLabel,
                        log
                )
        );
        if (accepted) {
            log.info("Received accepting verdict from " + judge + " for " + criteria);
        } else {
            throw new MojoFailureException("Received rejecting verdict from " + judge + " for " + criteria);
        }
    }
}

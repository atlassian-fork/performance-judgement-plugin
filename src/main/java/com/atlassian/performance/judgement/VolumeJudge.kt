package com.atlassian.performance.judgement

import org.apache.maven.plugin.logging.Log

internal class VolumeJudge(
        private val log: Log
) {

    fun judge(
            stats: InteractionStats,
            criteria: PerformanceCriteria
    ): Boolean {
        var accepted = true
        for (interaction in criteria.getInteractionKeys()) {
            val volume = stats.volumes[interaction]!!
            val minimumVolume = criteria.minimumVolume
            if (volume < minimumVolume) {
                val warning = "The volume of ${stats.cohort} $interaction is $volume," +
                        " which is under the minimum threshold of $minimumVolume"
                log.warn(warning)
                accepted = false
            }
        }
        return accepted
    }

}
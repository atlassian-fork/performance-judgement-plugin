package com.atlassian.performance.judgement

import org.apache.maven.plugin.logging.Log
import java.time.Duration

internal class PerInteractionJudge(
        private val log: Log
) {

    fun judge(
            criteria: PerformanceCriteria,
            baselineStats: InteractionStats,
            experimentStats: InteractionStats
    ): Boolean {
        var accepted = true
        for (interaction in criteria.interactions) {
            val key = interaction.key
            val baselineLocation = baselineStats.locations[key]!!
            val experimentLocation = experimentStats.locations[key]!!
            val baselineDescription = "'${baselineStats.cohort}' location $baselineLocation"
            val experimentDescription = "'${experimentStats.cohort}' location $experimentLocation"
            val tolerantBaselineLocation = Duration.ofNanos(
                    (baselineLocation.toNanos() * (1 + interaction.toleranceRatio)).toLong()
            )
            if (experimentLocation > tolerantBaselineLocation) {
                log.warn("The $experimentDescription is a regression against $baselineDescription for $interaction")
                accepted = false
            } else {
                log.info("The $experimentDescription fits under $baselineDescription for $interaction")
            }
        }
        return accepted
    }
}